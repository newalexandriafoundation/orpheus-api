# build environment
FROM node:9.6.1 as builder

# set working directory
RUN mkdir /app
COPY . /app/.
WORKDIR /app

# install and cache app dependencies
RUN yarn

# build
RUN yarn build


# production environment
FROM keymetrics/pm2:latest-alpine
RUN mkdir /app
COPY --from=builder /app ./app
WORKDIR /app
# rebuild node to support google cloud vision
RUN npm rebuild 
RUN ls
CMD ["pm2-runtime", "start", "pm2.json", "--env", "production", "--web"]