import _s from 'underscore.string';

// services
import PermissionsService from './PermissionsService';

// models
import Item from '../../models/item';
import Project from '../../models/project';

// errors
import { AuthenticationError, PermissionError, ArgumentError } from '../errors';


/**
 * Logic-layer service for dealing with tags
 */

export default class TagService extends PermissionsService {
	/**
	 * Count total tags
	 * @param {string} projectId
	 * @returns {number} count of tags
	 */
	async count({ projectId }) {
		const tags = await Item.distinct('tags', { projectId });

		if (!tags) {
			return 0;
		}

		return tags.length;
	}

	/**
	 * Get a list of all tags in project
	 * @param {string} projectId
	 * @returns {String[]} array of tags
	 */
	async getTags({ projectId }) {
		return await Item.distinct('tags', { projectId });
	}
}
