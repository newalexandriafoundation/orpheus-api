import _s from 'underscore.string';

// services
import PermissionsService from './PermissionsService';

// models
import Item from '../../models/item';
import Project from '../../models/project';

// errors
import { AuthenticationError, PermissionError } from '../errors';


/**
 * Logic-layer service for dealing with fields
 */

export default class FieldService extends PermissionsService {

	/**
	 * Get a list of fields
	 * @param {string} projectId
	 * @returns {Object[]} array of fields
	 */
	async getFields({ projectId }) {
		const args = {};
		const fields = [];

		if (!projectId) {
			return [];
		}

		if (projectId) {
			args.projectId = projectId;
		}

		const labels = await Item.distinct('metadata.label', { projectId, });
		labels.forEach((label) => {
			fields.push({
				name: label,
				values: [],
				type: ''
			});
		});

		const items = await Item.find(args);

		items.forEach((item) => {
			item.metadata.forEach((metadatum) => {
				if (['text', 'number', 'date'].indexOf(metadatum.type) >= 0) {
					fields.forEach((field) => {
						if (field.name === metadatum.label) {
							if (!field.type) {
								field.type = metadatum.type;
							}
							if (field.values.indexOf(metadatum.value) < 0) {
								field.values.push(metadatum.value);
							}
						}
					});
				}
			});
		});

		fields.push({
			name: 'Tags',
			values: await Item.distinct('tags', { projectId }),
		});

		return fields;
	}

}
