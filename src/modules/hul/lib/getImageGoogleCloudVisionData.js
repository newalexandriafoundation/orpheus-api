import vision from '@google-cloud/vision';


const client = new vision.ImageAnnotatorClient();


const getImageGoogleCloudVisionData = async (imageUrl) => {
	const data = {};

	const results = await client.labelDetection(imageUrl);

	// image color identification
	let imageProperties = [];
	try {
		imageProperties = await client.imageProperties(imageUrl);
	} catch (error) {
		console.error(error);
	}
	
	return { 
		labels: results[0],
		imageProperties: imageProperties[0],
	};
};


export default getImageGoogleCloudVisionData;
