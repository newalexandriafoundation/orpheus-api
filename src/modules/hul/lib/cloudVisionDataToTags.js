import { slugify } from 'underscore.string';

const annotationsToTags = (annotations) => {
	const tags = [];

	annotations.forEach((annotation) => {
		tags.push(slugify(annotation.description.toLowerCase()));
	});

	return tags;
};


const cloudVisionDataToTags = (cloudVisionDataRaw) => {
	let tags = [];

	if (cloudVisionDataRaw && cloudVisionDataRaw.labels) {
		const labels = cloudVisionDataRaw.labels;
		tags = tags.concat(annotationsToTags(labels.landmarkAnnotations));
		tags = tags.concat(annotationsToTags(labels.logoAnnotations));
		tags = tags.concat(annotationsToTags(labels.labelAnnotations));
		tags = tags.concat(annotationsToTags(labels.textAnnotations));
	}

	return tags;
};


export default cloudVisionDataToTags;
