

// models
import Item from '../../../models/item';


const makeItemTitleFromCollection = async (collection) => {
	let title = '';

	const existingCollectionItems = await Item.find({ collectionId: collection._id });
	const itemNumber = existingCollectionItems.length + 1;

	title = `${collection.title} (${itemNumber})`;

	return title;
};

export default makeItemTitleFromCollection;
