import axios from 'axios';
import winston from 'winston';

import Manifest from '../../../models/manifest';


const saveManifest = async (item, files) => {
	const images = [];

	if (!files.length) {
		return null;
	}

	files.forEach((file) => {
		let newImageName = file.name;
		newImageName = newImageName.replace(`${file._id}-`, '');

		images.push({
			_id: file._id,
			name: newImageName,
			label: file.title,
		});
	});

	// update item manifest
	const manifest = {
		itemId: item._id,
		title: item.title,
		label: item.title,
		description: item.description || '',
		attribution: 'The Charlie Archive at the Harvard Library',
		images,
	};

	let existingManifest = await Manifest.findOne({ itemId: manifest.itemId });
	if (!existingManifest) {
		existingManifest = new Manifest(manifest);
		await existingManifest.save();
		existingManifest = await Manifest.findOne({ itemId: manifest.itemId });
	} else {
		await Manifest.update({
			itemId: manifest.itemId,
		}, {
			$set: manifest
		});
	}

	manifest._id = existingManifest._id;
	const manifestCreationResult = await axios.post('http://generate-manifests.orphe.us/manifests', {
		manifest: JSON.stringify(manifest),
		responseUrl: process.env.MANIFEST_RESPONSE_URL,
	});

	return true;
};

export default saveManifest;
