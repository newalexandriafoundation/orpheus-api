import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM

import Collection from '../models/collection';


// build Type Composer with model and options
const customizationOptions = {}; 
// Project renamed to Archive after Orpheus.MVP, model name change not practical
const CollectionTC = composeWithMongoose(Collection, customizationOptions); 


// exports
export default CollectionTC;

