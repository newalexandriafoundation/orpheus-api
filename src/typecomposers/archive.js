import { composeWithMongoose } from 'graphql-compose-mongoose'; // GraphQL Composer with Mongoose ORM
import { schemaComposer } from 'graphql-compose';

import Project from '../models/project';
import Item from '../models/item';

import CollectionTC from './collection';
import ItemTC from './item';


// build Type Composer with model and options
const customizationOptions = {};
// Project renamed to Archive after Orpheus.MVP, model name change not practical
const ArchiveTC = composeWithMongoose(Project, customizationOptions);

const MetaFieldValueTC = schemaComposer.createObjectTC({
	name: 'MetaFieldValueType',
	description: 'meta field value',
	fields: {
		name: 'String',
		count: 'Int',
	},
});

const MetaFieldsTC = schemaComposer.createObjectTC({
	name: 'MetaFieldsType',
	description: 'meta fields',
	fields: {
		name: 'String',
		values: [MetaFieldValueTC],
	},
});

const ItemsTC = schemaComposer.createObjectTC({
	name: 'ItemsType',
	description: 'Item list and meta fields',
	fields: {
		itemList: [ItemTC],
		itemMetaFields: [MetaFieldsTC],
	},
});

const FieldITC = schemaComposer.createInputTC({
	name: 'FieldInputType',
	description: 'Metadata Field Schema base query type',
	fields: {
		name: 'String',
		values: '[String]',
	},
});

ArchiveTC.addRelation(
	'collections',
	{
		resolver: () => CollectionTC.getResolver('findMany'),
		prepareArgs: {
			filter: source => ({ projectId: source._id }),
		},
		projection: { projectId: true },
	}
);

ArchiveTC.addFields({
	items: {
		type: ItemsTC,
		args: {
			itemFilter: [FieldITC],
			limit: { type: 'Int', defaultValue: 10 },
			offset: { type: 'Int', defaultValue: 0 },
		},
		resolve: async (source, { itemFilter, limit, offset }, c, d) => {
			const projectID = source._id;
			const modelArgs = {};
			const metaArgs = {};

			// build model filter
			if (projectID) {
				modelArgs.projectId = projectID;
				metaArgs.projectId = projectID;
			} else {
				return {
					itemList: [],
					itemMetaFields: [],
				};
			}
			if (itemFilter) {
				modelArgs.$and = [];
				itemFilter.forEach((filterParam) => {
					modelArgs.$and.push({
						metadata: {
							$elemMatch: {
								label: filterParam.name,
								value: { $in: filterParam.values },
							},
						},
					});
				});
			}

			// fetch item list

			const itemList = await Item.find(modelArgs).skip(offset).limit(limit);


			// build meta table
			const labels = await Item.distinct('metadata.label', {
				projectId: metaArgs.projectId,
			});

			const metafields = [];
			labels.map((label) => {
				metafields.push({
					name: label,
					values: [],
				});
			});

			// populate all possible meta fields
			const items = await Item.find(metaArgs);
			items.map((item) => {
				item.metadata
					// exclude some types
					.filter(metadatum => ['text', 'number', 'date'].includes(metadatum.type))
					// populate each field.label
					.map((metadatum) => {
						// populate each field.values
						metafields.map((metafield) => {
							// type match and de-dup
							if (
								(metafield.name === metadatum.label) &&
								(metafield.values.filter(value => value.name === metadatum.value).length === 0)
								) {
								metafield.values.push({
									name: metadatum.value,
									count: 0,
								});
							}
						});
					});
			});

			/*
			fields.push({
				name: 'Tags',
				values: await Item.distinct('tags', {
					projectId: metaArgs.projectId
				}),
			});
			*/

			// counter ++
			const itemListAll = await Item.find(modelArgs);
			itemListAll.map((item) => {
				item.metadata.map((meta) => {
					metafields.map((field) => {
						if (field.name === meta.label) {
							field.values.map((value) => {
								if (value.name === meta.value) {
									value.count += 1;
								}
							});
						}
					});
				});
			});

			// exclude empty metafields
			const itemMetaFields = metafields.filter(field => field.values.length > 0);

			return {
				itemList,
				itemMetaFields,
			};
		},
	}
});


/*
// TBD: should we lift itemFilters to the top level so that Items and ItemMetaFields are siblings?
ArchiveTC.setResolver(
	'findOne',
	ArchiveTC.getResolver('findOne').addFilterArg({
		name: 'itemFilter',
		type: [FieldITC],
	}),
);
*/


// exports
export default ArchiveTC;

