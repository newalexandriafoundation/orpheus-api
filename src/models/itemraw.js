import mongoose from 'mongoose';
import shortid from 'shortid';

// plug-ins
import timestamp from 'mongoose-timestamp';

const Schema = mongoose.Schema;

const ItemRawSchema = new Schema({
	_id: {
		type: String,
		default: shortid.generate
	},
	sourceId: {
		type: String,
		required: true,
		index: true,
	},
	rawData: {
		type: Schema.Types.Mixed,
	}
});


// add timestamps (createdAt, updatedAt)
ItemRawSchema.plugin(timestamp);


const ItemRaw = mongoose.model('ItemRaw', ItemRawSchema);

export default ItemRaw;
export { ItemRawSchema };
