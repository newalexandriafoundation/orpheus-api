/**
 * concat a list of color meta to a list of item meta with de-dup/overwrite
 * 
 */

const concatItemColorMetaDeDup = (listItemMeta, listColorMeta) => {
	listColorMeta.map((colorMeta) => {
		let found = false;
		listItemMeta.map((itemMeta) => {
			// same RGB code
			if ((itemMeta.type === 'color') && (itemMeta.value === colorMeta.value)) {
				itemMeta = colorMeta; // found and overwrite
				found = true;
			} 
		});
		if (!found) {
			listItemMeta.push(colorMeta);
		}
	});
	return listItemMeta;
};

export default concatItemColorMetaDeDup;
