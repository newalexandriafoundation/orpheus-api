/**
 * get a list of color names from a list of color objects from vision ai
 * 
 */
import Coloraze from 'coloraze';

const coloraze = new Coloraze();

const getColorName = colorCode => coloraze.name(colorCode);

export default getColorName;
